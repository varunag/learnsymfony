<?php

namespace mindfire\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use mindfire\EventBundle\Entity\Event;

class LoadEvents implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $event1 = new Event();
        $event1->setName('Darth\'s Birthday Party');
        $event1->setLocation('DeathStar');
        $event1->setTime(new \DateTime('tomorrow noon'));
        $event1->setDetails('I Got You');
        $manager->persist($event1);


        $event2 = new Event();
        $event2->setName('Killing Party');
        $event2->setLocation('Mars');
        $event2->setTime(new \DateTime('Friday Noon'));
        $event2->setDetails('Kill em All');
        $manager->persist($event2);

        $manager->flush();
    }
}